---
lang: fr
---

# ![](ressources/logo.jpeg) Complément web - JavaScript

### IUT Montpellier-Sète – Département Informatique

## TD7
#### _Thème : Le réactif en JavaScript_

Commencez par `git clone` le *fork* du TD7 que nous avons fait pour vous. Ce
*fork* doit se trouver dans
https://gitlabinfo.iutmontp.univ-montp2.fr/r4.01-developpementweb/etu/votre_login_IUT/TD7

# INTRODUCTION
Les TDs de JavaScript vous ont offert une bonne vue d'ensemble de JS (même s'il y a de très nombreuses subtilités que nous n'avons pas pu aborder). En pratique, les sites web modernes utilisent très fréquemment des frameworks basés sur JS plutôt que le JS "nature" sans framework (aussi appelé "Vanilla JS"). On peut citer parmi les plus populaires React (Facebook...), Angular (Google...) et Vue (GitLab...). Une des fonctionnalités proposées par une grande partie de ces frameworks est la "réactivité", c'est-à-dire, la possibilité de lier une variable à un élément du DOM pour que quand la première variable change, cela déclenche automatiquement une mise à jour du DOM. De manière plus générale, on peut aussi lier une variable à une autre variable... 
_(changement d'une variable → évènement → action : modification DOM ou changement d'une autre variable)_

Cela permet de séparer plus facilement la logique du site (ou de l'application) et la gestion de l'interface. Par exemple, dans une architecture MVC, la programmation réactive facilite les échanges entre vue et modèle. Vous avez déjà utilisé des mécanismes similaires en JavaFX (reposant sur les observables/observateurs), mais la stratégie d'implémentation sera différente ici.

Dans ce TD nous allons écrire notre propre petite bibliothèque réactive. Elle sera donc limitée, mais on va voir qu'en une cinquantaine de lignes de code, il est déjà possible de créer un outil relativement puissant. Cette bibliothèque permet deux choses :

  1. Déclarer des objets "réactifs" et enregistrer des "effets" associés aux objets réactifs. À chaque fois qu'une propriété d'un objet réactif change, les effets associés sont appliqués. 
  2. Déclarer directement dans le HTML que le contenu de certaines balises correspond au contenu d'une variable ou au résultat d'une expression. Ce contenu sera *réactif*, c'est-à-dire qu'il sera mis-à-jour automatiquement à chaque changement de la variable réactive.

Pour cela, notre bibliothèque fournira trois fonctions :

  1. `reactive(monObjet, nomDeObjet)` prend en argument l'objet `monObjet` et une *string* `nomDeObjet`. Elle renvoie une version réactive de `monObjet` (qu'il faudra utiliser à la place de `monObjet`). Elle enregistre aussi `nomDeObjet`, que l'on pourra utiliser pour faire référence à l'objet dans le HTML,
  1. `applyAndRegister(maFonction)` applique `maFonction` et enregistre toutes les dépendances réactives de `maFonction`. Ainsi, `maFonction` sera automatiquement réappliqué dès qu'une de ses dépendances est modifiée,
  1. `startReactiveDom()` permet de relier le contenu de certains éléments du DOM à des variables JS.

Pour bien comprendre ce qu'on fait, vous allez commencer par utiliser la version de la bibliothèque que nous allons vous fournir avant d'écrire votre propre version de cette bibliothèque.
Nous vous fournissons une version minifiée de la bibliothèque. C'est-à-dire, qu'elle a été transformée par un outil ([uglify](https://www.npmjs.com/package/uglify-js)) pour en diminuer la taille, mais elle devient alors illisible. L'utilisation d'outil de [minification](https://en.wikipedia.org/wiki/Minification_%28programming%29) est très répandue pour les fichiers `.js` et `.css` puisqu'elle permet de réduire considérablement l'encombrement du réseau, mais nous n'en parlerons pas plus dans le contexte de ce cours.

# Utiliser notre bibliothèque réactive
## EXERCICE 1 - Un premier exemple simple

Nous allons commencer par une utilisation très simple de notre bibliothèque.
Ouvrez la page `exempleSimple/exempleSimple.html`.
Cette page contient des boutons + et - qui modifient la longueur, la largeur et la hauteur d'un pavé droit (*cuboid* en anglais) et affiche le volume correspondant.
Elle affiche aussi un petit rectangle jaune dont la hauteur et la largeur sont la hauteur et la largeur du *cuboid* (avec l'échelle *10px* pour *1m*).
Vous allez simplifier cette page en utilisant notre bibliothèque réactive.

Prenez d'abord le temps de comprendre comment fonctionne cette page.
Notez que contrairement à ce qu'on faisait en JS "basique", on s'autorise à utiliser l'attribut `onclick` dans le HTML, ce qui est cohérent avec le fait qu'on va ensuite s'autoriser à mettre des variables JS directement dans le HTML.

### Inclure la bibliothèque
La première chose à faire est d'inclure la bibliothèque `reactiveMinified.js`. Cependant, `reactiveMinified.js` est un module et nous verrons comment utiliser correctement un module à la fin de l'exercice. Pour l'instant, on va simplement inclure `reactiveMinifiedNotModule.js` dans le *header* de notre fichier HTML avant les inclusions de l'autre fichier JS.

On va ensuite rendre notre *cuboid* réactif en utilisant la fonction `reactive(monObjet)`. Pour cela, il suffit de modifier la définition de *cuboid* qui ressemblait à

  ```js
  let cuboid = {length:2,...};
  ```
  pour qu'elle devienne

  ```js
  let cuboid = reactive({length:2, ...}, "cuboidHTML");
  ```
La variable `cuboid` contient maintenant un objet réactif. Finalement, on va rajouter à la fin du fichier `cuboid.js` la ligne

```js
startReactiveDom();
```
Normalement, la page doit continuer de fonctionner comme avant et nous pouvons commencer à simplifier notre page.

### Connecter le DOM à notre objet dynamique
Notre bibliothèque permet d'enrichir notre code HTML à l'aide de nouveaux attributs.
Le premier de ces attributs est l'attribut `data-textvar="monObjetReactif.maVariable"`qui indique que le `textContent` de cet élément HTML doit être égal à la propriété `maVariable` de l'objet `monObjetReactif` et ça de manière réactive. Le nom auquel `monObjetReactif` fait référence ne correspond pas au nom de la variable dans le JS, mais au nom donné dans l'appel à `reactive(monObjet, nomDeObjet)`.

Par exemple, on peut ajouter l'attribut `data-textvar="cuboidHTML.length"` à la balise `<span id="cuboidLength">` pour indiquer à notre bibliothèque que le contenu de ce *span* (le `textContent`) doit rester égal à `cuboid.length`.
On peut alors supprimer la ligne des fonctions `incrLength` et `decrLength` qui met à jour ce *span* puisque c'est fait automatiquement.

1. Faites-le, et vérifiez que le site continue de fonctionner. On peut aussi supprimer l'id du *span* qui est devenu inutile. On peut finalement supprimer du HTML le contenu initial du *span* puisqu'il est aussi automatiquement initialisé avec la valeur initiale de `cuboidHTML.length`.

1. Faites la même chose pour *width* et *height*.

Pour le volume, la situation semble plus délicate. On pourrait ajouter une propriété `volume` au *cuboid* qui se met à jour automatiquement lors de la modification de l'une des autres propriétés et associer le *span* du DOM à cette propriété.
Nous allons utiliser une autre solution. Au lieu d'utiliser l'attribut `data-textvar`, on va utiliser l'attribut `data-textfun` qui met le résultat d'une fonction dans le `textContent`. La syntaxe est `data-textfun="monObjetReactif.maMéthode()"` (on peut aussi mettre un argument entre les parenthèses).
Par exemple, on peut écrire une méthode volume de *cuboid* qui renvoie le volume du *cuboid* et l'utiliser dans le HTML `data-textfun="cuboidHTML.volume()"`.

1. Faites-le, puis supprimez la fonction `setVolume()` et les appels à cette fonction. On peut aussi supprimer l'id `volume` du HTML ainsi que le contenu initial du *span* (24) qui sont devenus inutiles. Vérifiez que le site fonctionne toujours correctement.

*Note :* Le travail de notre bibliothèque est de détecter automatiquement que le résultat de cette fonction dépend des trois propriétés de l'objet réactif *cuboid* et qu'il faut donc mettre à jour le contenu en rappelant la fonction dès que l'une des propriétés du *cuboid* change.

### Modifier le style dynamiquement
Le style de notre rectangle jaune dépend aussi des propriétés `width` et `height` du *cuboid*.
Plutôt que de modifier le style à la main dès qu'on change `width` ou `height`, on va utiliser un autre attribut HTML de notre bibliothèque.
L'attribut `data-stylefun` permet d'associer de manière réactive un style à une fonction.
La valeur qu'on doit donner à cette variable est une fonction qui retourne un objet dont les propriétés sont les propriétés CSS qu'on veut changer. 

<!-- 
Remarque : 
Mettre les choses à faire dans des listes (à défaut de <div class="exercise">) pour qu'on les voie bien 
-->

1. Afficher dans votre console le style du rectangle jaune pour vous remémorer le contenu de la propriété style.

1. Commencez par retirer du JS les deux lignes qui changent le style. Ensuite, ajoutez à notre objet *cuboid* la méthode suivante `monStyle: function() {return {width:'100px',height:'340px'};}` et ajoutez l'attribut `data-stylefun="cuboidHTML.monStyle()"` à la balise du rectangle jaune. Si tout fonctionne bien, le rectangle jaune a maintenant les dimensions indiquées dans la variable `monStyle`, mais il ne change plus quand on modifie `width` et `height`. 

   **Attention** : ne pas supprimer l'id du rectangle cette fois puisqu'elle est aussi utilisée par le CSS.

2. Remplacez la méthode `monStyle` par une fonction `dimensionDuRectangle` qui renvoie un style qui dépend des propriétés `width` et `height` du *cuboid* et utilisez le style dans la balise du rectangle jaune. Le site devrait fonctionner normalement à nouveau.

3. On peut simplifier le CSS en supprimant les deux lignes qui donnent une dimension initiale au rectangle jaune. On peut aussi supprimer du JS la définition de la variable `yellowRectangle`.

### Utiliser le module

Pour l'instant, pour avoir un fichier JS qui dépend d'un autre fichier JS, il fallait tous les inclure dans le HTML dans le bon ordre. 
On aimerait plutôt que chaque fichier JS indique ses dépendances (comme on a
l'habitude de faire dans d'autres langages). 
Les [modules](https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Modules) permettent de faire cela. Officiellement, les modules sont arrivés en JavaScript avec ES6. Il existe cependant plusieurs versions différentes, non-officielles, plus anciennes, des modules. Heureusement, quasiment tous les navigateurs savent utiliser la version des modules de ES6 et c'est donc celle que nous allons utiliser.
<!-- 
La page qui indique 0.08% des sites date de 2018.
Pour des statistiques actuelles, il faut cliquer sur le lien (qui indique 8%)
https://chromestatus.com/metrics/feature/timeline/popularity/2062

En fait, les gens utilisent des bundler genre webpack.
Les bundlers faisaient tout le boulot de compiler les JS en 1 seul fichier.
Ce qui explique les statistiques.

Maintenant, d'autres bundler comme Vite renvoient vers des modules JS
-->
L'utilisation des modules sur le web reste pour l'instant assez limitée (d'après [Google](https://v8.dev/features/modules#adoption) seulement 11% des pages utilisent `<script type="module">`), mais ils gagnent tout de même en popularité (et ils sont aussi utilisés pour les usages JS hors du navigateur).

Attention, les navigateurs ont une forte tendance à garder les modules en cache. Ça implique un chargement plus efficace des pages, mais ça implique aussi qu'un simple `F5` pourrait ne pas rafraichir le JS, il faudra privilégier `Ctrl+F5` qui met la page à jour en ignorant le cache.

Pour inclure un module dans le HTML, on va utiliser le type `module` au lieu du type `text/javascript`.
Le fichier HTML ainsi inclus pourra utiliser le mot-clef `import` pour importer le contenu d'un autre fichier. Il y a de nombreuses autres différences, les principales sont :

- le fichier sera interprété en [mode strict](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Strict_mode) ;
- la portée du module est limitée au module (une variable globale définie dans un module n'est pas accessible dans le fichier HTML par exemple) ;
- pas besoin d'utiliser l'attribut `defer`, car le chargement est automatiquement différé ;
- pour éviter de rencontrer des erreurs CORS (pour des raisons de sécurité), on ne pourra plus accéder à notre site en passant par `file://`, mais il faudra passer par un serveur web.

Les deux principaux mots clefs liés aux modules sont `import` et `export` qui permettent de définir les fonctionnalités importées et exportées par un module.

1.  Commencez par inclure `cuboid.js` dans le HTML en tant que module en changeant le type en `type="module"`.
	Supprimez la ligne qui inclut le script `reactiveMinifiedNotModule` du HTML.
	On peut alors utiliser `import` dans notre fichier `cuboid.js`. Ajoutez en haut du fichier la ligne qui importe les trois fonctions qu'on utilise :
    
	```js
	import {applyAndRegister, reactive, startReactiveDom} from "./reactiveMinified.js";
	```
	Testez votre site et essayez de comprendre d'où vient l'erreur rencontrée (regardez dans la console des outils de développement). 
    
2.  Puisque `cuboid.js` est maintenant importé en tant que module, la variable cuboid n'existe que dans la portée du module et donc elle n'est pas accessible quand on écrit `onclick="cuboid.decrLength()"`.
	Il y a plusieurs moyens de contourner ce problème :
	1. D'abord, un module peut quand même accéder à la portée globale (et en particulier à l'objet [window](https://developer.mozilla.org/fr/docs/Web/API/Window)), mais c'est une solution peu recommandée ici. 
	2. Une meilleure solution, serait d'ajouter les listeners sur les éléments du DOM dans notre module `document.getElementById(...).addEventListener('click',...)`.
	3. On va utiliser une troisième solution : notre bibliothèque définit un attribut `data-onclick=monObjet.maMéthode` qui prend une fonction avec la même syntaxe que `data-textfun` et associe cette fonction au clic sur cet élément. Réparez le site en remplaçant l'utilisation de `onclick` par `data-onclick=...` (n'oubliez pas que le `cuboid` a été enregistré sous le nom `cuboidHTML` par la fonction `reactive` et c'est ainsi ce nom-là qu'il faut utiliser). Vérifiez que tout fonctionne.

*Remarque :* Sachez qu'il existe [plusieurs syntaxes différentes](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Statements/import) pour utiliser `import`, mais nous n'entrerons pas dans ces détails ici. Il existe aussi [une fonction `import()`](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/import) qui fait un import dynamique et qui peut être utilisée hors d'un module (ce qui permet notamment d'ajouter progressivement l'utilisation des modules à un ancien projet JavaScript).

Voilà, nous avons réécrit notre petit exemple avec notre bibliothèque réactive. À la place d'utiliser un objet défini à la volée, on aurait pu définir une classe. Cependant, nous allons voir, dès le prochain exercice, que l'objet qu'on rend réactif doit respecter certaines contraintes. En particulier, toutes ses propriétés doivent être **immuables** (si ce mot est mystérieux pour l'instant, pas d'inquiétude, nous reviendrons dessus).


## EXERCICE 2 - Réalisation d'un morpion
Le morpion (tic-tac-toe en anglais) et la TODO liste sont les deux exemples les plus souvent utilisés pour illustrer le fonctionnement des différentes bibliothèques réactives.
À cause de sa simplicité, notre bibliothèque se prête moins à la réalisation d'une TODO liste… nous allons donc réaliser un morpion.

Dans le dossier `morpion`, vous trouverez `tic-tac-toe.html`.
Vous pouvez ouvrir la page dans votre navigateur. Pour l'instant le jeu s'affiche, mais rien ne fonctionne.
Nous vous fournissons un fichier CSS qu'il n'y aura pas besoin de modifier (vous êtes libre de l'améliorer).
Dans un premier temps, nous n'aurons pas besoin de modifier le HTML, mais seulement le JS.


1.  Prenez le temps de comprendre l'organisation du fichier HTML et du fichier JS.
	Notez que les classes utilisées dans le HTML ne servent qu'au CSS.
	Notre objet `game` est réactif et contient quatre propriétés :
  	* `board` : un tableau qui contient l'état des 9 cases du plateau :
          	- "" si la case est vide,
          	- "X" si X a joué dans cette case,
          	- "O" si O a joué dans cette case.
  	* `nextPlayer` : le nom du joueur qui doit jouer "O" ou "X". O commence.
  	* `winner` : l'identité du gagnant (`undefined` s'il n'y a pas encore de gagnant).
  	* `roundNumber`: le numéro du round actuel.
    
	La méthode `isWinner(P)` vérifie si le joueur P (P valant "O" ou "X") a gagné la partie.
 
1.  La méthode `play(i)` doit faire jouer le prochain joueur en case `i`, s'il est autorisé à le faire.
	Commençons par écrire une version qui ne fait pas de vérification et qui met uniquement le plateau à jour.
	On voudrait simplement écrire
 	 
	```js
  	game.play = function (i){
      	this.board[i] = this.nextPlayer;
  	}
	```
	Essayez de cliquer sur plusieurs cases du plateau. Que constatez-vous ?
	Affichez `game.board` dans le terminal du navigateur pour essayer de comprendre le problème. Le réactif ne fonctionne plus...
    
    
1.  En fait, pour fonctionner correctement, notre bibliothèque a besoin que le contenu des propriétés des objets réactifs soit "immuable". C'est-à-dire que je peux écrire quelque chose de la forme
    
    ```js
    monObjetReactif.maPropriété = nouvelleValeur;
    ```
    mais pas
    
    ```js
    monObjetReactif.maPropriété.maSousPropriété = nouvelleValeur;
    ```
    ni
    
    ```js
    monObjetReactif.maPropriété.methodeQuiChangeMaPropriété();
    ```
    
	La raison est que notre bibliothèque ne détecte que `monObjetReactif` est modifié que si on appelle un setter pour modifier l'une de ses propriétés. 
	Or, la deuxième expression utilise le getter de `monObjetReactif` sur `maPropriété`, puis le setter de `maPropriété` sur `maSousPropriété`.
	Certaines bibliothèques réactives contournent ce problème en faisant le choix de détecter l'utilisation des getters de manière récursive. D'autres, comme la nôtre (ou React, par exemple), font le choix de rendre immuables les propriétés des objets réactifs. Cette solution peut couter cher en performance si elle nous force à créer de nouveaux gros objets dès qu'on veut modifier une propriété, mais elle permet d'économiser beaucoup de performance pour le calcul des dépendances.
	Malheureusement, il n'y a pas de manière simple de vérifier que l'utilisateur respecte l'immuabilité des propriétés en JS natif, il va donc falloir le garder à l’esprit. ***Notre bibliothèque réactive ne fait des mises-à-jour que lorsqu'un setter est appelé.***
	
	Maintenant que le problème est identifié, on sait qu'il va falloir écrire une ligne qui ressemble à `game.board = laNouvelleValeur;`. On va donc faire une copie du tableau, modifier cette copie, puis remplacer game.board par cette copie. Attention, on ne peut pas se contenter d'écrire `let newBoard = game.board;` pour faire la copie. Si on fait cela, `newBoard` ne contient pas une copie de `game.board`, mais contient exactement le même tableau. Il y a de nombreuses syntaxes pour faire une copie d'un tableau en JS, on peut utiliser par exemple le spread operator `let newBoard = [...game.board];` (avant ES6, on utilisait slice:  `let newBoard = game.board.slice();`). On peut finalement compléter, notre fonction `play`:
    

    ```js
    game.play = function (i){
        let newBoard = [...this.board];
        newBoard[i] = this.nextPlayer;
        this.board = newBoard;
    }
    ```
	Normalement, vous pouvez maintenant mettre des "O" partout dans le plateau.

<!--
	***TODO commenter cette phrase*** Attention, il existe aussi des manières de modifier la valeur d'une propriété sans utiliser son setter, il faudra donc aussi s'interdire ces usages sur nos objets réactifs (par exemple attention au [spread opérateur](https://2ality.com/2016/10/rest-spread-properties.html#targets-with-setters), la syntaxe `objet= {...objet, ...lesTrucsÀChanger}` pose probablement des problèmes ici contrairement à `Object.assign(objet, lesTrucsÀChanger))`.
-->

2. Ajoutez ce qu'il faut à la fin de la fonction `play` pour que `game.nextPlayer` alterne de valeur entre "O" et "X" après chaque coup. Vérifiez que tout fonctionne.

3. Ajoutez ce qu'il faut au début de la fonction `play` pour que s'il y ait déjà une valeur autre que "" dans la case `i`, la fonction `return` sans modifier le jeu. Ajoutez aussi une ligne qui incrémente le numéro de round de 1. Vérifiez que tout fonctionne.

4. On va désormais gérer la détection du gagnant. Écrivez la fonction `setWinner()` qui utilise `isWinner(P)` et qui donne à `game.winner` la valeur
      - "O" si "O" a gagné,
      - "X" si "X" a gagné,
      -  `undefined` sinon.
  
    Appelez cette fonction à la fin de `play()` et modifier le test du début de `play()` pour interdire de jouer quand un des joueurs a gagné. Vérifiez que tout fonctionne.

5.  Plutôt que de devoir appeler `setWinner()` quand on modifie le plateau, on aimerait bien que cela soit automatique (c'est ce qu'on appelle une *propriété calculée*). Pour cela, on va utiliser la dernière fonction de notre bibliothèque (que nous n'avons pas encore utilisée explicitement). La fonction `applyAndRegister(maFonction)` permet d'appliquer `maFonction` et de s'assurer qu'elle sera réappliquée dès qu'une des propriétés d'un objet réactif utilisée par cette fonction est modifiée. On peut donc supprimer l'appel à `setWinner()` dans `play(i)` et ajouter la ligne `applyAndRegister(() =>{game.setWinner()});` juste avant l'appel à `startReactiveDom()`. Vérifiez que tout fonctionne.

	*Remarque :* En faisant `applyAndRegister(game.setWinner)`, on pourrait avoir un problème si vous utilisez `this` dans `setWinner` (en JavaScript `this` dépend de la manière dont la fonction est appelée). Une autre solution serait de ne pas utiliser `this` dans nos méthodes, mais d'utiliser `game` directement. Une troisième manière de régler le problème serait d'écrire `applyAndRegister(game.setWinner.bind(game))` (`bind(game)` permet de forcer la valeur de `this` à être `game` pendant les prochaines exécutions de la fonction ciblée).
6.  On peut maintenant écrire la fonction `restart`. Il suffit de redonner à chacune des propriétés de `game` la valeur qu'elles doivent avoir en début de partie (sauf `winner`, puisqu'elle est calculée automatiquement à partir des autres valeurs). L'utilisation de la fonction [Object.assign](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Object/assign) rend cette tâche encore plus simple et élégante. Vérifiez que tout fonctionne.

Ça y est, notre morpion fonctionne.

### Voyage dans le temps
Pour illustrer les avantages du réactif (et de l'immuabilité), nous allons aller un peu plus loin en mettant en place un système de retour en arrière qui permet à tout moment de retourner à un round du passé.
On va donc afficher des boutons pour les rounds précédents qui permettent de retourner au round en question.

![](ressources/wayBack.png)

1.  On va commencer par ajouter les boutons. Pour cela, ajoutez le code suivant dans le HTML à la fin du `<aside>`.

    ```html
    <p>Revenir en arrière au round:
        <span data-htmlfun="game.backRoundButtons()"></span>
    </p>
    ```
    Remarquez que cette fois, nous utilisons `data-htmlfun` au lieu de `data-textfun`, la différence est que le résultat de l'expression va être mis dans le `innerHTML` au lieu du `textContent` ce qui nous permet de mettre du HTML qui sera interprété. Ajoutez les deux fonctions suivantes et prenez le temps de comprendre ce qu'elles font.
    
    ```js
    game.round = function(i){
    	console.log(`Bouton round${i} utilisé.`);
    }
    game.backRoundButtons = function (){
      	let i = 4;
      	return `<button data-onclick="game.round(${i})">${i+1}</button>`;
    }
    ```
  Notez qu'on peut utiliser nos attributs réactifs à l'intérieur du HTML généré. Ici, on utilise `data-onclick`, mais on pourrait ajouter une balise qui contient aussi un `data-textfun` ou même récursivement `data-htmlfun` (cependant, cette fonctionnalité est assez limitée dans cette minibibliothèque et pourrait être source de bug).
    
1.  Modifiez la fonction `backRoundButtons()` pour qu'elle affiche un bouton pour chaque round antérieur au round actuel (comme dans l'image).

2. On va maintenant devoir mettre en place le retour en arrière. Pour cela, on va simplement sauvegarder en mémoire l'état du jeu après chaque round et restaurer l'état en question lors du clic sur un de ces boutons.
Puisque les propriétés de `game` sont immuables, on sait qu'on peut les stocker directement par simple copie et il suffira de les recopier dans l'autre sens pour revenir en arrière. 

   Ajoutez en haut du fichier JS le tableau
   `let previousState = [{},{},{},{},{},{},{},{},{}];` dont la n-ème case stocke l'état du plateau après le n-ème round.

   Dans la fonction `play()`, ajouter ce qu'il faut pour que les propriétés de `game` soient copiées dans l'objet `previousState[this.roundNumber-1]` avant de modifier `roundNumber`. Plutôt que de copier les propriétés une par une à la main, on pourrait utiliser [Object.assign](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Object/assign) à nouveau.

1. Il faut maintenant écrire la fonction `round()` pour qu'elle remette `game` dans le bon état. On peut à nouveau utiliser `Object.assign`. Vérifiez que tout fonctionne.
   <!-- Attention, round reçoit un string i, ce qui pose soucis si on fait roundNumber: i+1 -->

2. Appréciez l'instant.

On pourrait facilement améliorer notre morpion en enrichissant un peu notre CSS et en utilisant des couleurs pour X et O (par exemple, avec `data-stylefun`).


# Écrire notre bibliothèque réactive

Nous allons maintenant travailler dans le dossier *reactiveLib* qui contient une correction du premier exercice sur le *cuboid*.
Le fichier `cuboid.js` contient la correction du premier exercice et notre but va être de compléter la bibliothèque qui est contenue dans le fichier `reactive.js`. 
Vous allez principalement modifier `reactive.js`, mais prenez le temps d'expérimenter dans la console pour bien comprendre ce que vous faites.

1.  Commencez par vérifier que la page fonctionne pour l'instant. Puis remplacez la ligne

    ```js
    import {applyAndRegister, reactive, startReactiveDom} from "./reactiveMinified.js";
    ```
    par
    
    ```js
    import {applyAndRegister, reactive, startReactiveDom} from "./reactive.js";
    ```
    pour pouvoir commencer à écrire votre version de la bibliothèque dans le fichier `reactive.js`. Quelle erreur constatez-vous en regardant la console du navigateur ?

1.  L'erreur obtenue dans la console du navigateur nous indique que le fichier `cuboid.js` essaie d'importer des variables qui ne sont pas exportées par `reactive.js`. Or le concept des modules est justement que **seules les variables/fonctions/objets/classes exportées dans un fichier sont importables dans un autre fichier**. On va donc devoir exporter nos trois fonctions dans le fichier `reactive.js`.
    Pour cela, on peut simplement ajouter à la fin du fichier `reactive.js` la ligne
    
    ```js
    export {applyAndRegister, reactive, startReactiveDom};
    ```
    Encore une fois, il y a [d'autres syntaxes intéressantes pour le mot clef `export`](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Statements/export) que nous ne verrons pas en détails. Retenez qu'on peut importer dans un fichier ce qui a été exporté dans l'autre. Cela permet d'avoir un contrôle plus fin sur la portée des variables.

La question des modules est réglée et le message d'erreur dans la console devrait disparaitre. Nous allons maintenant pouvoir nous concentrer sur l'écriture de notre bibliothèque, pour faire fonctionner le site.    
Cette bibliothèque contient trois fonctions principales :

   - `reactive(myObject)` qui prend un objet en argument et le rend réactif,
   - `applyAndRegister(effect)` qui applique la fonction `effect` et détecte toutes ses dépendances réactives pour que cet effet soit appliqué à nouveau dès qu'une des dépendances change,
   - `startReactiveDom()` qui parcourt le DOM pour relier les éléments qui contiennent un `data-textval`, `data-htmlval` ou `data-style` à leur valeur.

   
Nous allons commencer par compléter la plus simple de ces fonctions `startReactiveDom()`.

## La fonction startReactiveDom
Avant de coder quoi que ce soit, prenons le temps de nous interroger sur nos attributs HTML personnalisés (`data-textvar` ou `data-onclick` par exemple).
Comment se fait-il que je puisse inventer mon propre nom d'attribut en HTML ?
Ce sont des [attributs de données](https://developer.mozilla.org/fr/docs/Learn/HTML/Howto/Use_data_attributes).
On peut ajouter des attributs de la forme `data-xxx` à n'importe quelle balise HTML.
La valeur de l'attribut `data-truc` est accessible en JavaScript en écrivant `monElement.dataset.truc`.

1.  Dans la console du navigateur, essayez d'afficher le contenu de l'attribut `data-stylefun` du rectangle jaune (que vous pouvez sélectionner avec `document.querySelector("#yellowRectangle") `).

1.  Dans la fonction `startReactiveDom`, il va falloir détecter tous les éléments du DOM qui possèdent un de nos attributs spéciaux et les traiter. Nous avons déjà écrit les lignes qui gèrent `data-onclick`. Vous pouvez vérifier qu'en cliquant sur les boutons de `length` un message s'écrit dans la console.
    
	Essayons de décortiquer cette fonction ensemble : 
	1. D'abord, `document.querySelectorAll` permet de récupérer la liste des éléments du DOM qui correspondent à un sélecteur. Le sélecteur `[monAttribut]` sélectionne tous les éléments du DOM qui ont l'attribut `monAttribut`.
	Notre boucle `for(let ... of...)` fait donc une boucle sur tous les éléments du DOM qui ont un attribut `data-onclick`.
    
	2. L'application de `maChaine.split(/[.()]+/)` sur une chaine de caractère renvoie un tableau qui correspond au découpage de `maChaine` à chaque occurrence de l'un des trois symboles `.`, `(` ou `)` donné en argument. Ainsi, si la string contenue dans `elementClickable.dataset.onclick` a la forme `objet.methode(argument)`, alors la ligne
    
       ```js
       const [nomObjet, methode, argument] = elementClickable.dataset.onclick.split(/[.()]+/);
       ```
       découpe la *string* en trois morceaux qu'elle place dans les trois variables `nomObjet`, `methode` et `argument`. Pour mieux comprendre, vous pouvez temporairement ajouter les lignes
       
       ```js
       console.log(elementClickable.dataset.onclick);
       console.log([nomObjet, methode, argument]);
       console.log ("***************");
       ```
       après cette ligne.

	3. Il faut ensuite ajouter un `eventListener` sur l'élément cliquable qui va exécuter cette méthode de cet objet avec le bon argument. Par exemple, on veut que sur l'élément

       ```html
       <button data-onclick="cuboidHTML.decrLength()">
       ```       
       cette fonction exécute quelque chose de similaire à
       ```js
       elementCliquable.addEventListener('click', (event) => { cuboid.decrLength() });
       ```
       La principale difficulté est de récupérer l'objet `cuboid` à partir de son nom `cuboidHTML`. La *map* `objectByName` associe chaque nom d'objet à l'objet correspondant. En JavaScript, on ajoute un élément dans une *map* avec `maMap.set(key, value)` et on lit une valeur avec `maMap.get(key)`. Pour l'instant la seule chose que fait la fonction `reactive(passiveObject, name)` est d'enregistrer le nom de l'objet avec l'objet correspondant. Donc si on connait le nom de l'objet on peut récupérer l'objet correspondant avec la ligne 
       
       ```js
       const objet = objectByName.get(nomObjet);
       ```
	
	4. Finalement, en JS pour accéder à une propriété d'un objet les syntaxes `monObjet.maPropriété` et `monObjet["maPropriété"]` sont équivalentes. Donc si `methode="decrLength"`, `objet[methode]` est équivalent à `objet.decrlength`.
	
	   *Remarque :* l'argument est ici une chaine de caractère, donc il faut penser à prendre ça en compte dans l'écriture de la méthode (par exemple, la méthode pourrait avoir besoin de le reconvertir en entier). Pour l'instant, notre méthode doit avoir un ou zéro argument, il faut une astuce supplémentaire pour gérer le cas avec un nombre arbitraire d'arguments.

2.  Essayez de compléter la fonction `startReactiveDom` pour faire la même chose pour `data-textfun`. Il faut écrire une seconde boucle, qui liste tous les éléments du DOM qui ont un attribut `data-textfun` et qui remplit leur `textContent` avec le résultat de la fonction.
	Vérifiez que votre solution fonctionne : le volume devrait maintenant s'afficher correctement au chargement de la page.
    

1.  Normalement, votre code permet d'initialiser le `textContent` des différents `data-textfun`.
	Nous aimerions bien faire plus que l'initialisation, mais que cette instruction soit réappliquée dès qu'une des dépendances réactives est modifiée.
	Pour cela, on va utiliser `applyAndRegister`, ce qui devrait donner le code suivant (avec en bonus la correction pour `data-textvar` qui est très similaire):
 	 
	```js
  	for (let rel of document.querySelectorAll("[data-textfun]")){
        const [obj, fun, arg] = rel.dataset.textfun.split(/[.()]+/);
        applyAndRegister(()=>{rel.textContent = objectByName.get(obj)[fun](arg)});
  	}
  	for (let rel of document.querySelectorAll("[data-textvar]")){
        const [obj, prop] = rel.dataset.textvar.split('.');
        applyAndRegister(()=>{rel.textContent = objectByName.get(obj)[prop]});
  	}
	```
	Au chargement de la page, vous devriez obtenir :
    
	![](ressources/startDOM1.png)
    
	Le chargement de la page fonctionne et les `data-onclick` sont utilisés correctement, mais nous n'avons pas commencé à mettre en place le réactif donc pour l'instant l'affichage est figé et ne suit pas l'évolution des variables.
	L'idée pour `data-htmlvar`, `data-htmlfun` et `data-stylefun` est quasiment identique, mais avec des petites subtilités que nous verrons plus tard.

	<!-- 
	Je me suis demandé pourquoi pas de applyAndRegister() dans data-onclick ?
	On pourrait faire une remarque : on ne doit pas "simuler un clic" si la variable change
	-->
## Enregistrement des effets et la fonction trigger

Dans la suite, on appelle **effets réactifs** les fonctions que l'on doit appeler quand une variable réactive change. 
Pour notre première version du réactif, nous allons stocker l'ensemble des effets enregistrés dans un ensemble (un [set](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Set)).
On crée un ensemble (`Set`) vide avec l'instruction `const monSet = new Set();`, on ajoute un élément avec l'instruction `monSet.add(nouvelElement)` et on peut parcourir le contenu d'un set avec la boucle `for (let element of monSet){...}`.
Voici un pseudo-code de la stratégie que nous allons utiliser.

```js
let registeredEffects; // l'ensemble des effets "réactifs"

function applyAndRegister(effect){
    // applique effect une première fois et l'ajoute à registeredEffects
}

function trigger(){
    // parcourt registeredEffects pour appliquer tous les effets enregistrés.
}

function reactive(passiveObject, name) {
    // 1. crée un reactiveObject à partir de passiveObject: son rôle est d'appeler trigger pour
    //    appliquer tous les effets réactifs enregistrés dès qu'il est modifié ;
    // 2. enregistre reactiveObject sous le nom name dans objectByName ;
    // 3. renvoie reactiveObject.
}
```

Pour l'instant, on ne va pas s'intéresser à la fonction `reactive` qui est la plus compliquée, et on va garder celle qui est dans le fichier. Nous allons d'abord nous occuper des deux autres fonctions.

1.	Déclarez en haut du fichier `registeredEffects`, une variable globale qui contient au début un set vide. Ajouter une ligne à la fonction `applyAndRegister` pour qu'elle ajoute la fonction `effect` à `registeredEffects`.
    
1.	Écrivez la fonction `trigger()` qui exécute toutes les fonctions enregistrées dans `registeredEffects`. Il suffit d'utiliser la boucle `for of` pour parcourir toutes les fonctions contenues dans `registeredEffects` et de les appliquer une-par-une.

	Pour tester le fonctionnement, ajouter temporairement la ligne
    `window.trigger=trigger;` après la définition de `trigger()` (elle nous donne accès à `trigger` en dehors du module dans la console). Appuyez sur plusieurs boutons de votre page, puis exécutez `trigger()` dans la console, et, si tout fonctionne, l'appel à `trigger()` devrait mettre la page à jour.
    
    La prochaine difficulté est de réussir à appeler `trigger()` au bon moment sans devoir le faire nous même dans la console du navigateur.

## L'objet proxy et la première version du réactif

On veut que dès qu'un objet réactif est modifié, la fonction `trigger()` soit appelée.
La difficulté principale est donc de détecter qu'un objet réactif est modifié. C'est là qu'intervient l'objet [proxy](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Proxy).

[MDN nous dit](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Proxy) : "Un objet Proxy permet de créer un intermédiaire pour un autre objet qui peut intercepter et redéfinir certaines opérations fondamentales pour lui."
Par exemple, il permet de redéfinir le comportement des getters, des setters, du constructeur...
La création d'un proxy prend deux paramètres, la cible (l'objet dont on modifie le comportement), et le gestionnaire (*handler* en anglais) qui redéfini le comportement de différentes opérations.
Considérons l'exemple suivant.

```js
const handler = {
    set(target, key, value){
        console.log("la propriété "+key+ " prend la valeur "+ value
            + " et son ancienne valeur était "+target[key]);
	},
};
let monObjet = new Proxy ({a:4, b:7}, handler);
```
 
<!-- Est-ce qu'on ne pourrait pas montrer les setter en JS avant d'attaquer Proxy ? -->

Ici, on redéfinit le comportement du *setter* dans le handler: `target` est l'objet dont on change une propriété, `key` est la clef de la propriété de cet objet qui est en train d'être modifié et `value` est la nouvelle valeur.

1. Entrez l'exemple dans la console du navigateur. Vérifiez qu'on peut lire
   `monObjet.a` et `monObjet.b`. 
2. Essayez de changer la valeur de `monObjet.a`. Vous n'avez pas
   besoin d'appeler un *setter* explicitement car l'instruction `monObjet.a=32;`
   appelle implicitement le *setter* de la propriété `a` de `monObjet`. 
   
   Que se passe-t-il ? La valeur a-t-elle changé ?

   On a bien détecté que le *setter* avait été appelé, mais la valeur de `monObjet.a` n'a pas changé, puisque le *setter* que nous avons redéfini ne change pas la valeur.

3. Modifions l'exemple précédent pour que le *setter* fasse son travail correctement.

	```js
	const handler ={
		set(target, key, value){
			console.log("la propriété "+key+ " prend la valeur "+ value
			+ " et son ancienne valeur était "+target[key]);
			target[key] = value;
		},
	};
	let monObjet = new Proxy ({a:4, b:7}, handler);
	```

   Vérifiez que l'on peut maintenant changer la valeur de `monObjet.a` et
   `monObjet.b`. 

4.  En fait, un setter doit normalement renvoyer un booléen qui indique si l'opération s'est déroulé correctement. On va donc plutôt utiliser la version suivante.

	```js
    const handler ={
        set(target, key, value){
            console.log("la propriété "+key+ " prend la valeur "+ value
            + " et son ancienne valeur était "+target[key]);
            target[key] = value;
            return true;  
        },
    };
    let monObjet = new Proxy ({a:4, b:7}, handler);
    ```

 <!--   
Pour des raisons techniques, une meilleure version du code précédent est la
suivante :

```js
const handler ={
    set(target, key, value, receiver){
        console.log("la propriété "+key+ " prend la valeur "+ value
        + " et son ancienne valeur était "+target[key]);
		// semblable à const result = (target[key]=value);
        const result = Reflect.set(target, key, value, receiver);
        return result;  
    },
};
let monObjet = new Proxy ({a:4, b:7}, handler);
```

On renvoie vraiment le bon booléen. De plus, utiliser `receiver` et `Reflect.set` permet un meilleur comportement dans un certain nombre de cas particuliers (par exemple, si le setter de l'objet en argument avait déjà été modifié et utilise this. ).


Vu que notre bibliothèque est imparfaite, enlevons ce bout s'il résout un problème que nous ne rencontrerons pas 
-->

4.  Il faut maintenant que `trigger` soit appelée automatiquement dès qu'une propriété d'un objet réactif change.
	C'est là que proxy intervient. Modifiez la fonction `reactive` en s'inspirant de l'exemple de proxy précédent pour qu'elle :
	-  définisse un *handler* dont le setter appelle `trigger()` après chaque modification d'une propriété,
	-  crée dans une variable `reactiveObjet` un proxy de `passiveObjet` avec le *handler*,
	-  enregistre `reactiveObjet` dans `objectByName` (jusqu'à maintenant elle enregistrait `passiveObject`),
	-  renvoie `reactiveObjet`.
    
	Attention : 
	1. dans le setter, il faut faire l'appel à `trigger` **après** la mise à jour de la valeur (sinon, on va mettre à jour la page avant de changer la valeur). 
	2. bien faire un `return` de `reactiveObject`.

Ça y est, notre site fonctionne ! Prenez le temps de vérifier que vous comprenez ce que vous avez fait et profitez du moment... mais pas trop longtemps, car nous allons voir qu'il y a encore un gros problème.


## Détecter les dépendances pour améliorer le réactif
Avec la version actuelle de notre bibliothèque, dès qu'une propriété d'un objet réactif change, on réapplique tous les effets enregistrés.
À la place, on voudrait mettre à jour uniquement les effets qui dépendent de la propriété modifiée.
La raison la plus évidente est l'optimisation de performance (imaginez un site avec de nombreux objets réactifs et de nombreux effets enregistrés).
Il y a une seconde raison plus subtile. 

1. Pour s'en rendre compte, nous allons changer la manière de calculer le volume. Nous allons supprimer la fonction `cuboid.getVolume()` et lire plutôt l'attribut `cuboid.volume`. Pour faire ceci, remplacez dans le HTML 
   ```html
   <h1>Volume: <span data-textfun="cuboidHTML.getVolume()"></span>m³</h1>
   ```
   par 
   ```html
   <h1>Volume: <span data-textvar="cuboidHTML.volume"></span>m³</h1>
   ```
   et décommentez la ligne suivante dans `cuboid.js`
   ```js
   applyAndRegister(()=>{cuboid.volume = cuboid.length * cuboid.width * cuboid.height});
   ```   
   Que se passe-t-il quand on ouvre le site ? Pourquoi ?

Quand une propriété de `cuboid` est modifiée, `cuboid.volume` est mis à jour, mais `cuboid.volume` est une propriété de `cuboid` donc `cuboid.volume` est mis à jour, mais `cuboid.volume` est... Heureusement, le navigateur détecte cette récursion infinie.

Nous allons donc devoir détecter les dépendances correctement. En effet, la mise à jour de `cuboid.volume` devrait uniquement déclencher la mise-à-jour des `effect` qui dépendent de `cuboid.volume`. Ici, ce genre d'utilisation semble superflu, mais, souvenez-vous, dans le morpion la propriété `game.winner` nous permettait de recalculer le gagnant seulement quand le jeu change plutôt qu'à chaque fois qu'on l'utilise.

1.    Commentez la ligne de `cuboid.js` qui pose un problème pour le moment (le volume ne s'affiche plus, mais le reste fonctionne à nouveau). Vous pouvez la décommenter à la fin de chaque question pour comprendre ce qu'il se passe, mais recommentez la ensuite.

2.  Créez la fonction suivante :

    ```js
    function registerEffect(effect, target, key){
        console.log("on enregistre un effet sur la clef " + key);
        registeredEffects.add(effect);
    }
    ```
    Remplacez temporairement la ligne correspondante de la fonction `applyAndRegister` par un appel à `registerEffect(effect, null, null)`.
    
Notre objectif va maintenant être de :

  -  Faire un appel à `registerEffect(effect, target, key)` pour chaque `target` et chaque `key` tel que `target[key]` est utilisée dans `effect`.
  -  Modifier `registerEffect` pour qu'elle enregistre ces informations.
  -  Modifier `trigger` pour qu'elle n'appelle que les effets qui dépendent du `target[key]` qui vient d'être modifié.
 
Cette partie est particulièrement délicate et astucieuse, prenez donc bien le temps de bien faire les consignes une par une au fur-et-à-mesure.

1.  Commençons par ajouter la variable `let registeringEffect = null;` tout en haut du fichier.
	Cette variable va contenir l'effet qu'on est en train d'enregistrer ou `null` si l'on n'est pas en train d'enregistrer un effet.
	On va donc modifier `applyAndRegister` pour gérer correctement cette variable :
 	 
    ```js
    function applyAndRegister(effect){
        registeringEffect = effect;
        effect();
        registeringEffect = null;
    }
    ```
1.  On va maintenant modifier la fonction `reactive` en ajoutant une fonction get au *handler* qui devient :
 	 
    ```js
    const handler ={
        get(target, key){
            if(registeringEffect !== null)
                registerEffect(registeringEffect, target, key);
            return target[key];  
        },
        set(target, key, value){
            target[key] = value;
            trigger();
			return true;
        },
    };
    ```

	Profitez-en pour constater le fonctionnement du getter de proxy. Ouvrez votre page et vérifiez que tout fonctionne.
    
	Prenez le temps de comprendre ce qu'il se passe en regardant le résultat dans la console.
	Remarquez que la fonction `applyAndRegister` lance l'effet, mais ne l'enregistre plus directement.
	<!-- Remarquez que pendant l'enregistrement d'un effet la fonction `applyAndRegister` lance l'effet, mais ne l'enregistre plus. -->
	Dans le getter de l'objet réactif, si jamais un effet est en cours d'enregistrement, on enregistre que cet effet dépend de la propriété qui est en train d'être `get`. Donc maintenant, `registerEffect` est appelée pour chaque objet réactif et chaque propriété de cet objet qui est utilisé (plus précisément qui est `get`) par l'effet qui est en train d'être enregistré. Nous sommes ainsi capables de détecter exactement les dépendances, il va désormais falloir les enregistrer.

 
2.  On peut simplifier légèrement le code.
	La fonction `registerEffect` a aussi accès à la variable `registeringEffect`. On peut donc retirer son argument effect et utiliser directement `registeringEffect`. Vérifiez que tout fonctionne comme avant.

Il faut décider comment on va stocker les dépendances. On a besoin de garder pour **chaque** propriété de **chaque** objet réactif un `Set` des effets dépendant de cette propriété. On va donc utiliser une `Map` de `Map` de `Set`.

1.  Commencez par définir en haut du fichier `let objetDependencies = new Map();`. Cette `Map` stocke pour chaque objet réactif la `Map` de ses dépendances. On va initialiser la `Map` de chaque objet réactif lors de sa création par la fonction `reactive`:

    ```js
    function reactive(passiveObject, name){
      	objetDependencies.set(passiveObject, new Map());
      	...
    }
    ```
	Modifiez `registerEffect` pour qu'elle ajoute `registeringEffect` au set `objetDependencies.get(target).get(key)`.
	Attention, il faut vérifier si `objetDependencies.get(target).get(key)` est bien définie. Pour ça on peut utiliser `objetDependencies.get(target).has(key)` qui renvoie `true` si la clef existe. Sinon, il faut l'initialiser avec l'instruction `objetDependencies.get(target).set(key, new Set());`.

2.  Modifiez ensuite `trigger` pour qu'elle prenne deux arguments `trigger(target, key)` et qu'elle applique tous les effets enregistrés dans le set `objetDependencies.get(target).get(key)`. Attention, vérifiez que le `Set` est défini, sinon on peut se contenter de `return;` (on peut réutiliser la méthode `objetDependencies.get(target).has(key)`). 

    Il faut ensuite modifier l'appel à `trigger` dans `reactive` pour lui fournir `target` et `key`. On peut finalement retirer la variable `registeredEffects` qui ne sert plus à rien. Vérifiez que tout fonctionne.
	Décommentez la ligne qui causait une récursion infinie dans le fichier `cuboid.js` et vérifiez que tout fonctionne.


## Implémenter data-stylefun, data-htmlfun, data-htmlvar...
Débrouillez-vous! Voici quand même quelques indications.

**data-stylefun:**
Vous pouvez commencer par vous rappeler à quoi ressemble `element.style` pour un élément du DOM et comment fonctionne `data-stylefun` dans notre bibliothèque.
Ensuite, en vous inspirant de `data-textfun`, vous pourrez utiliser `Object.assign` pour faire fonctionner `data-stylefun`. Si vous faites ça correctement, le rectangle jaune devrait s'afficher.

**data-htmlfun (difficile !):**
Le fonctionnement est similaire à `textfun`, mais avec deux subtilités. Premièrement, il faut utiliser `innerHTML` à la place de `textContent`.

La seconde difficulté, c'est que si l'on veut pouvoir utiliser du `HTML` qui contient nos attributs spéciaux, il faut s'assurer qu'après chaque modification réactive du `HTML` les nouvelles dépendances potentielles sont détectées et que les anciennes dépendances qui correspondent à des variables qui n'existent plus sont supprimées.
On ne peut pas simplement relancer `startReactiveDom()`, sinon les dépendances seront enregistrées en double (ce qui va ralentir le site, mais aussi créer des bugs par exemple avec les *listeners* enregistrés en plusieurs exemplaires). Une solution simple, mais très inefficace, serait de séparer les dépendances du DOM des autres dépendances et de les recalculer à chaque fois (c'est-à-dire vider les dépendances enregistrées et lancer `startReactiveDom()`).

Une solution un peu plus efficace est de changer `startReactiveDom()` en `startReactiveDom(subDOM=document)` pour qu'elle prenne un argument optionnel. Cet argument nous dit dans quelle partie du DOM on doit détecter les dépendances (et donc utiliser `subDOM.querySelectorAll`). Ensuite, si l'on modifie le `innerHTML` d'un élément `element` on peut appeler `startReactiveDom(element)`. Pour éviter de détecter en plusieurs exemplaires certaines dépendances, il faudrait aussi faire tous les `querySelectorAll` de `startReactiveDom()` avant de modifier le `HTML`.


**Le morpion devrait maintenant fonctionner avec votre version de la bibliothèque.**

**data-srcvar ou  data-classfun:**
Vous pouvez aussi définir vos propres `data-truc` qui pourraient permettre de rendre d'autres arguments dynamiques. Pourquoi pas un `data-srcvar` qui permet de changer le lien d'une image ? Ou un `data-classfun` qui permet de changer la liste des classes d'un élément (pour celui-là, réfléchissez bien à la bonne manière de le faire).

**data-reactiveInput:**
Il est assez simple d'ajouter un attribut qui permet de lier le contenu d'une variable à un champ de texte `input`. Le fonctionnement est proche de celui de data-onclick. On peut utiliser un `listener` pour détecter le changement de valeur de l'`input` et recopier la nouvelle valeur de l'`input` dans la bonne variable.

Vous pourriez aussi rajouter un certain nombre de tests et de messages d'erreurs (par exemple, pour vérifier que les attributs dans le dataset ont le bon format).


## Bonus
 
1.  Remplacer la ligne `let objetDependencies = new Map();` par `let objetDependencies = new WeakMap();`. Tout devrait continuer de fonctionner. Les [WeakMaps](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap) sont presque comme des maps, sauf qu'elles sont ignorées par le garbage collector. C'est-à-dire que si un objet n'existe que dans des weakMap, la mémoire est libérée, c'est donc légèrement plus efficace. L'idée étant que s'il n'existe que dans la weakMap, c'est qu'on en a plus besoin donc on peut l'oublier.
  
2. Pour l'instant les fonctions qu'on peut utiliser dans nos attributs dans le HTML, ne peuvent utiliser qu'un ou zero argument. On veut améliorer cela pour accepter un nombre arbitraire d'arguments. Pour cela on peut modifier légèrement l'utilisation de `split` et utiliser [`apply`](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Function/apply) (ou encore mieux, le [spread operator](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Spread_syntax#utiliser_la_d%C3%A9composition_dans_les_appels_de_fonction)).

## Conclusion
Voilà, nous avons écrit notre petite bibliothèque réactive.
Elle est très simple, mais permet déjà un certain nombre de possibilités.
Attention, notre bibliothèque est peu sécurisée.
Cependant, utiliser et écrire cette librairie vous donne une petite idée de ce qu'il y a derrière le réactif et vous permettra d'en faire une meilleure utilisation le jour où vous utiliserez un vrai framework JS réactif.

De nombreuses stratégies différentes sont utilisées pour implémenter le réactif en JS. Notre stratégie basée sur l'utilisation de Proxy est inspirée de la stratégie de [Vue](https://vuejs.org/) (comme expliqué [ici](https://vuejs.org/guide/extras/reactivity-in-depth.html#how-reactivity-works-in-vue)), mais est évidemment très simplifiée en comparaison.






