import {applyAndRegister, reactive, startReactiveDom} from "./reactiveMinified.js";
let previousState = [{},{},{},{},{},{},{},{},{}];
let game = reactive({board: Array(9).fill(""), nextPlayer: "O", winner: undefined, roundNumber: 1}, "game");

game.getboard = function (i) {
    return this.board[i];
}

game.isWinner = function (P) {
    const winLines = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];
    for (let line of winLines) {
        if (game.board[line[0]] === P && game.board[line[1]] === P && game.board[line[2]] === P)
            return true;
    }
    return false;
}

game.setWinner = function () {
    if (game.isWinner("O")) {
        game.winner = "O";
    } else if (game.isWinner("X")) {
        game.winner = "X";
    } else {
        game.winner = undefined;
    }
}

game.play = function (i) {
    if (this.board[i] !== "") {
        return;
    }
    Object.assign(previousState[this.roundNumber] ,game);
    let newBoard = [...this.board];
    newBoard[i] = this.nextPlayer;
    this.board = newBoard;
    if (this.nextPlayer === "O") {
        this.nextPlayer = "X";
    } else {
        this.nextPlayer = "O"
    }
    this.roundNumber++;
}

game.restart = function () {
    game.board = Array(9).fill("");
    game.nextPlayer = "O";
    game.roundNumber = 1;
}
game.round = function (i) {
    game = Object.assign(game,previousState[i]);
}
game.backRoundButtons = function () {
    let str = "";
    for (let i = 1 ; i < game.roundNumber ; i++){
       str += `<button data-onclick="game.round(${i})">${i}</button>`;
    }
    return str;
}

applyAndRegister(() => {
    game.setWinner()
});
startReactiveDom();
