import {applyAndRegister, reactive, startReactiveDom} from "./reactive.js";

let cuboid = reactive({
  length: 2, 
  width : 3,
  height: 4,  
  getVolume: function (){ 
    return this.length * this.width * this.height;
  },
  incrLength: function (){
    this.length++;
    console.log(`la longueur augmente, sa nouvelle valeur est ${this.length}.`);
  },
  decrLength: function (){
    this.length--;    
    console.log(`la longueur diminue, sa nouvelle valeur est ${this.length}.`);
  },
  incrWidth: function (){
    this.width++;   
    console.log(`la largeur augmente, sa nouvelle valeur est ${this.width}.`);
  },
  decrWidth: function (){
    this.width--;    
    console.log(`la largeur diminue, sa nouvelle valeur est ${this.width}.`);
  },
  incrHeight: function (){
    this.height++;
    console.log(`la hauteur augmente, sa nouvelle valeur est ${this.height}.`);
  },
  decrHeight: function (){
    this.height--;   
    console.log(`la hauteur diminue, sa nouvelle valeur est ${this.height}.`);
  },
  dimensionDuRectangle: function() {
    return {width: this.width *10+'px', height:this.height*10+'px'};
  }
}, "cuboidHTML");
  
applyAndRegister(()=>{cuboid.volume = cuboid.length * cuboid.width * cuboid.height});

startReactiveDom();
