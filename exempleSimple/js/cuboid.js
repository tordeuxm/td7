import {applyAndRegister, reactive, startReactiveDom} from "./reactiveMinified.js";

let cuboid = reactive({
    length: 2,
    width: 3,
    height: 4,
    /*setVolume: function (){
      const areaElement = document.getElementById("volume");
      areaElement.textContent = this.length * this.width * this.height;
    },*/
    incrLength: function () {
        this.length++;

    },
    decrLength: function () {
        this.length--;

    },
    incrWidth: function () {
        this.width++;

        //yellowRectangle.style.width=10*this.width+"px";

    },
    decrWidth: function () {
        this.width--;

        //yellowRectangle.style.width=10*this.width+"px";

    },
    incrHeight: function () {
        this.height++;

        //yellowRectangle.style.height=10*this.height+"px";

    },
    decrHeight: function () {
        this.height--;

        //yellowRectangle.style.height=10*this.height+"px";

    },
    volume: function () {
        return this.height * this.width * this.length;
    },
    dimensionDuRectangle: function () {
        return {width: `${this.width}0px`, height: `${this.height}0px`};
    },
}, "cuboidHTML");
startReactiveDom();

